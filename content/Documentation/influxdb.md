---
title: "Influxdb Concepts - InfluxData Documentation"
date: 2020-03-09T08:37:15+02:00
draft: false
categories: ["Documentation"]
displayInList: true
---

- InfluxDB is a time series database designed to handle high write and query loads.
- It is an integral component of the TICK stack
  - T --> Telegraf
  - I --> InfluxDB
  - C --> Chronograf
  - K --> Kapacitor
- It is meant to be used as a backing store for any case involving large amounts of time-stamped data
- https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/ <-- A glossary of influxDB terms

### Key Features

- Custom high performance datastore written specifically for time series data
- Written in Go
- Simple, high performing write and querying for HTTP API's
- Plugin support for other data ingestion protocols
- Expressive SQL-like query language tailored to easily aggregate data
- Tags allow series to be indexed for fast and efficient queries
- - Retention policies efficiently auto-expire stale data
- Continuous queries automatically aggregate data to make frequent queries more efficient

## InfluxDB Key Concepts

### Database

- An influx Database is similar to a ttraditional relational database and serves as a logical container for users, retention policies, and time-series data
- Databases can have several users, continuous queries, retention policies, and measurements
- InfluxDB is a schemaless database which means it's easy to add new measurements, tags and fields any time

### Field Keys

- Field keys are strings and are the keys associated with field values

### Field Values

- Field values can be strings, floats, ints, or booleans
- Field values are associated with field keys and timestamps
- Queries that use field values as filters must scan all values that match your select statement, as a result those queries are not performant relative to queries on tags
- In general fields should not contain commonly-queried metadata

### Field Set

- The association of field keys and field values make up a field set
- Fields are a compulsory piece of the influxDB data structure

### Measurement

- A measurement acts as a container for the tags, fields and time column
- The measurement name is the description of the data stored in the associated fields
- Measurement names are strings
- The measurement is conceptually similar to a table
- A single measurement can belong to different retention policies

### Point

- A point refers to a single data record that has four components:
  - Measurement
  - tag set
  - field set
  - timestamp
- A point is uniquely identified by a timestamp and series

### Retention Policy

- A retention policy describes how long influxDB keeps data and how many copies of this data is stored in the cluster

### Series

- A series is a collection of points that share a measurement, tag set, and field key

### Tag Key

- Stored as strings and record metadata
- The keys associated with tag values\

### Tag Values

- Stored as strings and record metadata
- The values associated with tag keys

### Tag Set

- The different combinations of all the tag key and tag value pairs
- Tags are not compulsory, but it is a good idea to make use of them as they are indexed and make searching and querying more efficient
- Tags are ideal for storing commonly-queried metadata

### Timestamps

- Show the date and time, in RFC3339 UCT, associated with particular data
- Timestamps are stored in the time column of the database

- https://docs.influxdata.com/influxdb/v1.7/concepts/key_concepts/#timestamp <--- See an example of a simple influxDB database and the associated concepts

## InfluxDB schema Design and Data Layout

### General Recommendations

#### Encode Metadata in Tags

- Tags are indexed and fields are not indexed, therefore queries on tags are more efficient than queries on fields
- Store data in tags if they are commonly queried metadata
- Store data in tags if you plan to use them with <code>GROUP BY</code>
- Store data in fields if you are planning to use them with <code>InfluxQL function</code>
- Store data in fields if you *need* them to be something other than a string, tag values are always interpereted as strings

#### Avoid Using InfluxQL Keywords as Identifier Names

- This isn't necessary, but it simplifies writing queries, you won't have to wrap those identifiers in double quotes
- Note that you will also have to wrap keywords in double quotes if they contain numbers or special characters

### Discouraged Schema Design

#### Don't Have Too Many Series

- Tags containing highly variable information like UUIDs, hashes, and random strings will lead to a large number of series in the database, known as high series cardinality
- High series cardinality is a primary diver of high memory usage for many database workloads

#### Don't Use the Same Name for a Tag and a Field

- This often leads to unexpected behaviour when querying data

#### Don't Encode Data in Measurement Names

- It is better to differentiate data with tags than than with detailed measurement names

#### Don't Put More than One Piece of Information in One Tag

- Plitting a single tag with multiple pieces into separate tags will simplify your queries and reduce the need for regular expressions

### Shard Group Duration Management

#### Shard Group Duration Overview

- InfluxDB stores data in shard groups
- Shard groups are organized by retention policy and store data with timestamps that fall within a specific time interval called a shard duration
- If no shard group duration is provided, the shard group duration is determined by the retention policy's durationat the time the retention policy is created
- The default RP values are:

| RP Duration              | Shard Group Duration |
|--------------------------|----------------------|
| < 2 days                 | 1 hour               |
| >= 2days and <= 6 months | 1 day                |
| > 6 months               | 7 days               |

- The shard group duration is also configurable by RP

#### Shard Group Duration Tradeoffs

- Determining the optimal shard group duration means finding a balance between:
  - Better overall performance with longer shards
  - Flexibility provided by shorter shards

##### Long Shard Group Duration

- Longer shard durations allow InfluxDB to store more data in the same logical location
- This reduces data duplication, improves compression efficiency, and allows for faster queries in some cases

##### Short Shard Group Duration

- Shorter shard group durations allow the system to more efficiently drop data and record incremental backups
- WHen InfluxDB enforces RPs it drops entire shard groups, not individual datapoints, even if the points are older than the RP duration.
- A shard group will only be dropped when the shard group's duration end time is older than the RP duration

#### SHard Group Duration Recommendations

- Default shard group durations work well for most cases 
- High-thoughput or long running instances will benifit from longer shard group durations
- Recommendations for longer shard group durations:

| RP Duration           | Shard Group Duration |
|-----------------------|----------------------|
| <= 1 day              | 6 hours              |
| > 1 day and <= 7 days | 1 day                |
| > 7 days and =< 3 mos | 7 days               |
| > 3 mos               | 30 days              |
| infinite              | 52 weeks or longer   |

- Other factors to consider when setting up shard group duration:
  - Shard group durations should be twice as long as the longest time range of the most frequent queries
  - Shard groups should contain more than 100 000 points per shard group
  - Shard groups should each contain more than 1000 points per series

#### Shard Group Duration for Backfilling

- Bulk insertion of historical data covering a large time range in the past will trigger the creation of a large number of shards at once
- The concurrent access and overhead of writing to hundreds or thousands of shards can quickly lead to slow performance and memory exhaustion
- WHen writing historical data temporarily set up a longer shard group duration so fewer shards are created, typically a shard duration of 52 weeks works well for backfilling
