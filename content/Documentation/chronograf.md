---
title: "Chronograf - InfluxData Documentation"
date: 2020-03-10T09:32:26+02:00
draft: false
categories: ["Documentation"]
displayInList: true
---

- Chronograf is InfluxDB's open source web application
- It is used with other components of the TICK stack to visualize monitoring data and easiy create automation and alerting rules

## Key Features

### Infrastructure Monitoring

- View all hosts and their statuses in your infrastructure
- View the configured applications in each host
- Monitor your applications through Chronograf's preconfigured dashboards

### Alert Management

- Chronograf provides a UI for Kapacitor, InfluxDB's data processing framework
- Generate threshold, relative, and deadman alerts on your data
- Easily enable and disable existing alert rules
- View all active alerts on alert dashboard
- Send the alerts to the supported alert handlers

### Data Visualization

- Monitor your data with Chronograf's pre-created dashboards
- Create your own dashboards complete with various graph types and templates
- Investigate your data with data explorer and query templates

### Database Management

- Create and delete databases and retention policies
- View currently running queries and stop inefficient queries from overloading your system
- Create, delete and assign permissions to users

### Multi-organization and User Support

- Create organizations and assign users to those organizations
- Restrict access to administrative functions
- Allow users to setup and maintain unique dashboards for their organizations

# Using Pre-Created Dashboards in Chronograf

## Overview

- Preconfigured dashboards are delivered with CHronograf depending on which Telegraf plugins you have enabled and are available from the Host page list
