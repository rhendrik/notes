---
title: "Telegraf - InfluxData Documentation"
date: 2020-03-10T09:58:14+02:00
draft: false
categories: ["Documentation"]
displayInList: true
---

- Telegraf is a plugin-driven server agent for collecting and reporting metrics
- It is the first part of the TICK stack
- Telegraf has plugins to source a variety of metrics directly from the system it's runningon, pull metrics from 3rd party API's, etc
- It has output plugins to send metrics to a variety of other datastores, services, and message queues

## Key Features

- Written entirely in GO
- Minimal memory footprint
- Plugin system allows newer inputs and outputs to be easily added
- A wide number of plugins for a variety of many popular services

## Downloading

- https://portal.influxdata.com/downloads/

## Install & Basic Configuration

- Will require root privileges

### Networking

- Offers multiple service input Plugins that may require custom ports
- All port mappings can be edited in the config file

### NTP

- Telegraf uses the system's default UTC time to assign timestamps to data
- Use the Network Time Protocol (NTP) to syc time between hosts; if hosts clocks aren't synced with NTP the timestamps can be inaccurate

## Configuration

### Create Config File with Default Input and Output Plugins

- Every plugin will be in the file but most will be commented
```
telegraf config > telegraf.conf
```

### Create Config File with Specific Inputs and Outputs

```
telegraf --input-filter <pluginname>[:<pluginname>] --output-filter <outputname>[:<outputname>] config > telegraf.conf
```
# Getting Started

## Creating and editing the Config file

- Before starting the Telegraf server you need to edit and/or create the initial configuration that specifies your desired inputs and outputs
- There are several ways to create and edit the config file
- Here we will specify the inputs with the -input-filter flag and the outputs with the -output-filter flag
- The example below creates a config to specify two inputs:
  - one that reads metrics about the system's cpu usage (cpu)
  - one that reads metrics about the system's memory usage (mem)
```
telegraf -sample-config -input-filter cpu:mem -output-filter influxdb > telegraf.conf
```

## Start the Telegraf Service

```
sudo service telegraf start
```
## Results

- Once Telegraf is started it will start collecting data and writing it to the desired outputs

- See https://docs.influxdata.com/telegraf/v1.13/introduction/getting-started/ for an example of usage

# Concepts

## Telegraf Metrics

- Telegraf metrics are the internal representation used to model data after processing
- Closely based on InfluxDB's data model and contain four main components:
  - Measurement Name
    Description and namespace for the metric
  - Tags
    Key/value string pairs and usually used to identify the metric
  - Fields
    Key/value pairs that are typed and usually contain the metric data
  - Timestamp
    Date and time associated with the fields
- This metric type exists only in memory and must be converted to concrete representation in order to be transmitted or viewed 
- Telegraf provides output data formats (a.k.a serializers) for these conversions
- Telegrafs default serializer converts to InfluxDB Line Protocol, which provides a high performance and one-to-one direct mapping from Telegraf metrics

## Aggregator and Processor Plugins

![](image.png)
