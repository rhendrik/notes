---
title: "Docker Mastery With Kubernetes (Udemy)"
date: 2020-02-06T11:28:50+02:00
draft: false
categories: ['Udemy']
displayInList: true
---

## Overview of Course

- Requirements
- Install
- Containers
- Images
- Networking
- Volumes
- Compose
- Orchestration
- Swarm (redundant)
- Kubernetes

## Quick Commands

Docker commands can be executed in one of two ways: the old way and the new. The old way was simply <code>docker [COMMAND]</code>. however, there have been so many commands added since it's development that docker has created a new format to help specify and manage those commands. The new format is <code>docker [MANAGE] [COMMAND]</code> for example <code>docker run</code> can now be manged with <code>docker container run</code>. Both are correct and the optimal choice will vary from situation to situation.

- <code>docker version</code>
- <code>docker info</code>

## Image vs. Container

An image is the application we want to run. A container is a running instance of that image. You can have multiple containers running the same image. Docker's default image registry is Docker Hub.

<code>docker container run - -publish 8080:80 nginx</code>

- Download nginx image from dochub
- Start new container from that image
- Open port 8080, listen from port 80 on localhost, 8080 can be any port
- Routes trafic from the container IP to port 80

<code>docker container run - - publish 8080:80 - - detach nginx</code>

- Will output the unique container ID
- - - detach tells docker to run te container in the background as opposed to the foreground

## CLI Process Monitoring

<code>docker container top</code>

- The list of processs running in a container

<code>docker container inspect</code>

- The details of a single container's configuration


<code>docker container stats</code>



## Getting a Shell Inside Containers

<code>docker container run -it [IMAGE] bash</code>

- Start a new container interactively
- -t: sudo tty this simulates a terminal the way that SSH does
- -i: interactive keep session open to recieve terminal commands
- bash: tells the container to take commands from bash shell
- exin this shell with <code>exit</code>

<code>docker container exec -it [CONTAINER NAME] [PROGRAM TO RUN]</code>

- Run an additional command in an existing container

## Container Images, Where to Find them and How to Build Them

### What's in an Image (and What isn't)

- App binaries and dependencies
- Metadata about the image and hw to run it
- <cite>An image is an ordered collection of root filesystem changes and the corresponding execution parameters for use within a container - official definition</cite>
- It is NOT an OS. It has no kernel or kernel modules like drivers
- It is essentally an application as opposed to a full OS
- It can be as small as one file
- It can be as big as an ubuntu distro with a full lamp stack installed

## Using DockerHub Registry Images

### What's the Right Image to Use?

- Typically we always start with the official image. this will normally be the image with the most pulls. This will be the only image wth an 'official' written under it, as well as the only one without a forward-slash in front of it.
- All other images will have the account name of the user who built and uploaded them.
- Official images are images that are maintained and supported by the Docker organization
- Official images are the best way to start building images that cater to your specific needs
- Offcial images tend to have the best documentation
- Images are not named they are tagged, a version of an image can have more than one tag
- The latest tag will give you the latest version of a product. The default tag (if not specified) is the latest.
- If using an unofficial image, check the star rating and the number of pulls, popularity tends to establish a level of trust based on the experience of those that have used it

## Image Layers

- Images are designed using the Union Filesystem concept.
- Every image starts with a blank layer known as scratch.
- Every change that happens after that on the image, in the file system is its own layer.
- Some layers will have no change on the file size, others will be hundreds of megs
- Every layer gets it's own unique shaw that helps the system identify unique layers
- Each unique image layer is stored n the cache. Because each layer has it's own unique shaw no unique layer s ever stored more than once, this saves a lot of space
- this means that when uploading or downloading we never reupload or redownload the same layer twice
- If we have two images that are identcal but for one layer, each shared layer is only stored once.
- When running a container the cntaner itself is simply a layer that contains whatever pocesses are running inside the live container, layered on top f the original image
- The <code>docker history [IMAGE NAME]</code> command wll show all the changes made t the image in dockerhub
- <code>docker image inspect [IMAGE NAME]</code>
  - Returns JSON metadata about the image

## Image tagging and pushing to DockerHub

- <code>docker image tag</code>
  - Assigns 1+ tags to an image
- Imags don't technically have a name we can refer to them using one of three attrbutes:
  - Image repository name
      - Made up of the username or orginization name (if it's mot official) + '/' + 'repository'
      - Offical repositories live in the 'root namespace' of the repository, so they don't need an account name in front of the repository
  - Tag
      - Not a version and not a branch, it's a pointer to a specific image commit
      - There can be multple tags for the same image ID, these tags can be numbers (usually specfic versions) or names (usually a version wth a custom set up)
      - You can give an image a new tag with the <code>docker image tag [IMAGE NAME][:OPTIONAL TAG] [NEW TAG NAME]</code> command
      - The <code>latest</code> tag does not nevessarily mean the latest, it refers to the default, though when it comes to official tags t generally refers to the latest stable version of that tag
      - To upload a new tag to your DockerHub repository use the <code>docker image push [TAG NAME]</code> command
      - To upload an image with a new tag you need to be logged in using the <code>docker login</code> command
      - When uploading a new tag, any layers that have already been uploaded (so layers that your prevous tags have in common with your new tag) will not be uploaded again
      - if you'd like your tag to be uploaded to a private repository, create the repository on Dockerhub first and set it to private before uploading it.

## Building Images: the Dockerfile

 - A Dockerfile is a recipe for creating an image
- A Dockerfile has a similar syntax to a shell or bash script, but it is its own unique filetype that has been developed by docker for ths purpose
- The default name for a Dockerfile is Dckerfile wth a capital D
- If you want to use a Dockerfile other than the default you can do so by specifying it with the <code>-f</code> tag
- Dockerile comments start with a # on each line of comment
- Each stanza (command) is a layer on the new docker image that we are building
- Dockerfiles are exected top-down so the order f the stanzas do matter
- <code>FROM</code>
  - This cmmand s required to be n every Dockerfile
  - It normally specifies a minimal distribution to build your image from
- <code>ENV</code>
  - A way to set envirnment variables, this is the main way we set keys and values for running and building containers
  - The envionment variable is set so that any subseqent lines can use it
- <code>RUN</code>
  - This will execute shell commands as the container is being built, this is normall used for installing necessary packages and applications, unzipping, file edits etc, any commands that you can access at that point in the building process
  - This command has access to all the files and binaries installed on the release specified in <code>FROM</code>
  - Each iteration of this command has it's own layer, so any commands that you want to be executed in the same layer must be chained together with <code>&&</code>, this saves time and space
- <code>EXPOSE</code>
  - Bu default no ports will be exposed inside a docker container, this command will specify which port you would like to expose, e.g., 8080:80
  - This does not mean that the ports will automatically be open when we run the container, this is done using the <code>-p</code> flag when we run our container
- <code>CMD</code>
  - A required parameter, it is the final command that will be run everytime we launch a new container from the image, or everytime we restart a stopped container
- <code>WORKDIR</code>
  - THis is the best practise method used to  change directories as needed during the build process, do not use chained RUN commands for this
- <code>COPY</code>
  - This is the default command to copy source code from your host machine to your container

## Building Images: Running Docker Builds

- <code>docker image build -t [DESIRED NAME] [DESIRED PATH]</code>
- Build stepa are cached so that any installation steps do not need to be executed again the next time you build from that image
- Keep in mind that for each line in the Dockerfile that you change, each stanza executed after that will also change, this affects speed. E.g. f you are copying code in the first few lines of your Dockerfile, everytime that code is changed each following stanza will be executed again as opposed to just being taken from the cache
- Keep the things that change the least at the top of your dockerfile and the things tha change the most at the bottom to optimize build time

## Building Images: Extending Official Images

- Ideally you would use an official image as a base for your Dockerfile, as this will ensure the most stable version possible, they are the easiest to customize

## Container lifetime & Persistent Data

- Containers are usually immutable & ephemeral
- "immutable infrastructure: oly re-deploy containers, never change
- Ideally your container won't contain any unique data mixed in with its binaries
- Docker provides features to manage this separation of concerns
- When we remove a container it's UFS layer goes away
- aka persistent data
- There are two ways to do this: volumes and bnd mounts
- Volumes make a special location outside of the UFS; volumes are stored on the host as opposed to a layer on the image, this can be attached to any container
- Bind mount: link container path to host path


## Volumes

- <code>VOLUME</code> command in dockerfile
- Named volumes: a friendly way toassign volumes to containers
- <code>-v</code> allows us to either specify a new volume or to create a name for preexsting or default volumes <code>-v [VOLUME NAME]:[VOLUME PATH]</code>
- <code>docker volume create</code>
  - Required to do this before a docker run to use custom drivers and labels
  - This is the only way to specify a different driver

## Bind Mounting

- Maps a host fle or directory to a container file or directory
- Basically two locations pointing to the same place in memory
- Skips UFS and host files will overwrite container files
- You can't use this in a Dockerfile, it must be specified during <code>container run</code>
  - <code>run -v [FULL PATH NAME]:[DESIRED PATH IN CONTAINER]</code>
- To spot the difference between a named voluma and a bind mount: a bind mount starts with a '/'

## DockerHub: Digging Deeper

- Dockerhub is the most popular docker image regstry
- It's really docker registry with lightweight image building
- You can link git to hub and auto-build images on commit
- You can chain image building together

## Running Docker Registry

- it can be a registry that you store privately on your network
- Part of the docker/distribution github repo
- The defacto in private container registries
- Not as full featured as hub, no web UI, bastic authentication onlyo- At it's core t is simply a web API and storage system written in Go
- The registr needs o be secued wih TLS
- The registry storage needs to be cleaned up with Garbage Collection
- Hub caching can be enabled with te registry mirror

## Run a Private Docker Registry

- Run the registry on port 5000 by default
- Re-tag an existing image and push it into your new registry
- "Secure by default" - Docker won't talk to any registry without porper https unless it is running on a localhost
 For remote self-signed TLS, enable insecure registry in engine
- To push to a private registry:
  - <code>docker tag hello-world 127.0.0.1:5000/hello-world</code>
  - <code>docker push 127.0.0.1/hello-world</code>
  - Instead of pushing to docker hub docker will see the IP address ad push to that registry
- Run a private registry:
  - <code>docker container run -d -p 5000:5000 --name registry registry</code>

## What is Kubernetes?

- Kubernetes is a popular container orchestrator
- Orchestrator: Makes many servers act like one
- Releases in 2015 by google and maintained by an opensource community
- Rns on top of docker (usually) as a set of APIs in a container
- Provides API/CLI to manage containers accross servers
- Main tool: kubectl
- Many clouds provide kubernetes
- many vendors make a 'distribution' of it - similar to the concept of Linux distributions

## Kubernetes Architecture Terminology

### Basic Terms - System Parts

- Kubernetes/K8's/kubes - The whole orchestration system
- kubectrl: CLI to configure k8s and manage apps
  - Cube control is the offcial pronunciation
- Node: single server in the k8s cluster
- Kubelet: k8s agent running on nodes
- Control Plane/Master: Set of containers that manage the cluster
  - Includes the container running the API server, scheduller, control manager, etcd etc
- Kubernetes works with master(s) and nodes. 
- Masters and nodes will all be running on top of docker
- Master(s) are the control plane, all kubernetes management systems
- Your apps will generally be run in the nodes
- Each master will have containers to run: 
  - etcd: a distributed storage system for key value pairs
  - API: the way we talk and issue orders to the cluster
  - Scheduler: controls how ad where your containers are placed in the nodes
  - Controller manager: looks at the state of the whole cluster and uses the API to do that, takes the specs and compares the difference between what you're asking it to do and what's going on 
  - DNS controller
- Each node will have containers for:
  - A container for the kubelet agent
  - Kube proxy to control the networking

## Kubernetes Local Install

- Kubernetes is a series of containers, CLI's and configurations
- There are many ways to install, this is the simplest
- MicroK8s
  - Installs kubernetes on localhost
  - Uses snap
  - Control the service using microk8s.[COMMANDS] commands
  - Access kubectl with microk8s.kubectl, alias this to kubectl for convenience
- For learning in a browser:
  - play-with-k8s.com
  - katacda.com
  - Both are easy to get started with
  - Doesn't keep your environment

## Kubernetes Container Abstractions

- Pod: one or more containers running on one node
  - Basic unit of deployment
  - Containers are always run on pods
- Controller: for creating/updating pods and other objects
  - Many types of controllers
- Service: network endpoint that connects to a pod
- namespace: a way to filter your views in the CLI

## Kubernetes Run, Create, Apply

- Kubernetes is evolving and so is the CLI
- There are three ways to create pods from the kubectl-CLI:
  - <code>kubectl run</code> (changing to be only for pod creation reducing its scope, similar experience to docker run)
  - <code>kubectl create</code> (create some resources via CLI or YAML)
  - <code>kubectl apply</code> (create/update anything via YAML)

## Kubectl Run

### Creating pods with kubectl

- Check if kubectl is installed:
- <code>kubectl version</code>
  - Two ways to deploy pods: via commands or YAML
- Run an nginx web server:
  - <code>kubectl run myngnx -image nginx</code>
  - Ignore warning for now
- List the pods
  - <code>kubectl get pods</code>
  - Returns named object
- See all objects
  - <code>kubectl get all</code>
  - Lists all the objects
- Remove Deployment
  - <code>kubectl delete deployment mynginx</code>
  - Deletes everything created by the run command,deployments, replicasets ad pods


## Scaling ReplicaSets


- Start a new deployment for one replica/pod
  - <code>kubectl run myapache --image htpd</code> - Default run gives a single pod/replica
- Scale it up wth another pod
  - <code>kubectl scale deploy/my-apache --replicas 2</code> - deploy is an abbreviation of deployment, this can be done for a few commands, similarly the '/' is shorthand for a space
  - deloy = deployment = deployments
  - A <code>kubectl get all</code> will now show 2 pods and the desired field in the replicaset will also be set to 2

## Inspecting Kubernetes Objects

- <code>kubectl get pods</code>
- Get container logs
  - <code>kubectl logs deployment/myapache</code>
  - By default this logs command will only pull logs from one pod, not all of them
  - Adding - - follow will make t wait and look for any updated logs
  - Adding tail [number] will display the last [number] logs 
  - <code>kubectl logs -l run=myapache</code>
  - -l specifies the label, we're specifying the label that the run command applied
- Get a bunch of details about an object, including events
  - <code>kubectl describe pod/myapache-xxxx-yyyy</code>
  - get pods first to know the unique name of the pod you're looking for
  - If you don't specify which it will show all pods, this can be dangerous if there are many of them running
  - Pulls up information about different objects as well as events, this is a good way to get quick status of the reality of that object
- If you delete a lower level of abstraction (e.g. a pod) the higher levels of abstraction (deployment, replicaset), will make sure that that pod is recreated
- Watch a command (without needing watch)
  - <code>kubectl get pods -w</code>
- In a separate tab/window
  - <code>kubectl delete pod/myapache-xxx-yyy</code>
  - <code>kubectl get pods</code>
  - We will see the first pod being deleted, and a second get pods command will show that a new pod has been spun up

## Service Types
### Exposing Containers

- <code>kubectl expose</code> creates a service for existing pods
- A service is a stable address for pod(s)
- A service is an endpoint that is consistent so that things inside and outside the cluster might be able to access it
- When we create pods they don't automatically get a DNS name fr external connectivity with an IP address, if we want to connect to pods we need to create a service
- CoreDNS allows us to resolve services by name
- There are four different service types:
  - ClusterIP
    - The default
    - This is only available in the cluster, it's about one set of pods talking to another set of pods
    - Gets its own DNS address, this is the address that wll be in the CoreDNS control plane
    - Single, internal virtual IP allocated
    - Pods can reah service on apps port number
    - Always available in kubernetes
  - NodePort
    - Desugned for something outside the cluster to talk to your service through the IP addreses on the nodes themselves
    - When creating a NodePort you get a high port allocated on each node that's assigned to the service
    - Port is open on evey node's IP
    - Anyone can connect if they can reach the node
    - Other nodes need to be updated to this port
    - Always available in Kubernetes
  - LoadBalancer
    - Mostly used in the cloud
    - Controls an LB endpoint external to the cluster
    - Only available when infra provider gives you a loadbalancer (AWS, Azure etc)
    - Your infra provider will allow kubernetes to talk to their loadbalancer and configure them for you
    - Creates NodePort+ClusterIP and tells the LB to send to NodePort
    - This is only for traffic coming int your cluster from an external source
    - It requires an infra provider that allows kubernetes to talk to it remotely
  - ExternalName
    - Used less often
    - Not about inbound traffic to your services, this is about things in your cluster needing to talk to outside services
    - Adds CNAME DNS record to CoreDNS only
    - Not used for pods, but for giving pods a DNS name to use for something outside Kubernetes

## Creating a ClusterIP Service
