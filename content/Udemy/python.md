---
title: "Complete Python Bootcamp && Automate the Boring (Udemy)"
date: 2020-02-14T14:32:44+02:00
draft: false
categories: ['Udemy']
displayInList: true
---

## Course Overview

- Python 2vs Python 3
- Python Set-up
  - Python installation
  - Environment selection
  - Jupyter noteboks
- Object and Data Structure Basics
  - Numbers
  - Strings
  - Lists
  - Dictionaries
  - Tuples
  - Files
  - Sets
  - Booleans
- Comparson Operators
  - Basic Operators
  - Chained comparison operators
- Python Statements
  - If, elseif and else
  - For loops
  - While loops
  - Range
  - List comprehensions
- Methods and Functions
  - Methods
  - Functions
  - Lambda expressions
  - Nested statements
  - Scope
- Object Oriented Programming
  - Objects
  - Classes
  - Methods
  - Inheritance
  - Special methods
- Errors and Exeption Handling
  - Errors
  - Exceptions
  - Try
  - Except
  - Finally
- Modules and Packages
  - Creating modules
  - Installing modules
  - Exploring the python ecosystem
- Built in Functions
  - map
  - reduce
  - filter
  - zip
  - enumerate
  - all and any
  - complex
- Decorators in Python
- Python Generators
  - teration vs Generation
  - Creating generators

## Python 2 vs Python 3

- choosing between Python 2 and 3 used to be difficult because many companies had legacy python 2 code to be maintained, however python 2 has stopped recieving security updates in 2020
- Every major external python package has been updated to support python 3
- Going back to python 2 wll be easy, with some minor syntax changes

## Installing Python

- To install we will use the free Anaconda distro, this incudes python and some usefl libraries including Jupyter
- Install from anaconda.com/downloads

## Running Python Code

- There are several ways to run python code
- The three main types of environments are:
  - Text editors
    - General editors for any text file
    - Most of them can be customized
  - Full IDEs
    - Development environment designed specifically for python
    - Larger programs, only community editions are free
  - Notebook environments
    - Great for learning
    - See input and output next to eachother
    - Support markdown notes, media etc
    - Special file formats that are not .py
- to run a python script:
  - cd to the location of your file
  - python example.py
  - If you only run the python command it will open up a shell and let you enter code at the command line
    - quit with quit()

# Python Object and Data Structure Bascs

## Data Types

- Data types are building blocks when constructing larger pieces of code

| Name            | Type  | Description                            |
|-----------------|-------|----------------------------------------|
| Integers        | int   | Whole numbers                          |
| Floating Point  | float | Decimal numbers                        |
| Strings         | str   | Ordered list of chars                  |
| Lists/Arays     | list  | Ordered sequence of objects            |
| Dictionaries    | dict  | unordered key/value pairs              |
| Tuples          | tup   | Ordered immutable sequence of objects  |
| Sets            | set   | Unordered collection of unique objects |
| Booleans        | bool  | Value indicating true or false         |

- A dictionary is denoted by curly braces, a normal list is denoted by square braces
- You cannot change an object in a tuple, denoted by normal parentheses

## Python Numbers

- There are two main number types, ints and floats
- For addition: +, for subtraction: -, for multiplication: astersisk, for remainder: %

## Variable Assignments

- A variable is a name that has been assigned to a value of any data type
- Names can't start with a number and there can't be any spaces in them.
- Most symbols can't be used in variable names
- All names for now must be lowercase
- Don't use any special key-words
- Python uses dynamic typing, you can reassgn variables to different data types
- this makes python very flexible in its typing

   <code> my_dogs_name = 2</code>

   <code> my_dogs_name = ["sammy", "williams"]</code>
- Use type() to check the type of any variable
- To assign a variable <code> variable_name = value</code>
- Python allows you to reassign values with a reference to an object

  <code>a = a + a</code>

## Intro to Strings

- Strings are sequences of characters using either single or double quotes to delimit them
- If you have a single or double quote in the string iself wrap the string in the other kind of quote
- Because strings are ordered we can use indexing or slicing to grab sub-sections of the string

## Indexing and Slicing with Strings

- Indexing: grabbing a single character
- Slicing: grabbing a subsection
- To index a single character use the variable name and the index of the string in square brackets
  - <code>str[3]</code> <-- This will grab the 4th letter (3rd index) in the string str
- You can also index strings with negatives. e.g. "Hello World": 'r' sits at the 9th index, as well as the -3rd index
- To grab a slice of a string <code>str[STARTING INDEX:ENDING INDEX]</code>
  - Not specifying an ending index implies grab till the end of the string
  - Not specifying a starting index implies to start grabbing from the beginning of the string
  - Stopping index: up to but not including last index
  - str[::] <-- technically valid syntax for the whole string
  - This syntax can have a third parameter, step size
  - A step size of two will grab indices in steps of 2 (so every second letter)
  - Handy: str[::-1]  <-- This reverses the string

## String Properties and Methods

- Immutability: You cannot mutate or change
- Strings are immutable, so you can't grab an index and reassign it
- You can concatenate strings in python with the plus sign, you can also use multiplication
- Useful string methods:
  - Calling a method without () will nt call the method, it will just return what that method does
  - strname.methodname
  - x.upper() <-- transform string to uppercase, to make it affect the original string you have to reassign it to the method's output
  - x.lower()
  - x.split() <-- creates a list from a string, each indice is a ord, each word is delimeted by whitespace, or based on a character passed into the parenthesis
  - ','.join() <-- joins a list of strings with a comma
  - x.ljust(10, ' ') <-- pads the string to the left with as many spaces as needed to make the string the length specified
  - x.rjust(10, ' ') <-- pads the string to the right with as many spaces as needed to make the string the length specified
  - x.center(10, ' ') <- pads the strung to the center by adding as many spaces on either side as needed to make the string the length specified
  - x.strip() <-- strips whitespace from both sides of the string
  - x.lstrip() <-- strips whitespace from both the left of the string
  - x.rstrip() <-- strips whitespace from both the right of the string
  - x.replace('e', 'a') <== replaces every occurence of 'e' with 'a'

## String Formatting for Printing

- often yu'll want to inject a variable into a string for printing. You can for example use the + operator to concatenate inside the print function
- In general this concept is called string interpolation
- We'll explore two methods for this:
  - .format
  - f-strings (formatted string literals)
- .format
  - The string will be defined and inside the string there will be curly braces to indicate where you want to inject variables, think of printf
  - print("Hello {}, mellow {}".format("fellow", "fellow"))
  - We can also specify whick injection goes where by specifying the index in our curly braces
  - print("The {2} {1} {0}".("fox", "brown", "quick"))
  - We can also assign key pair values to our injected strings, this can be much more readable
   - print("The {q} {b} {f}".(f="fox", b="brown", q="quick"))
- We can use %s inside a string to insert other strings into a string
```
print('Hello %s, you are invited to a %s at %s at %s. Please bring %s' % (name, event, place, time, food))
```

## Escape Characters

- Some characters need to be escaped when put into a string.

| Escape Character | Prints as    |
|------------------|--------------|
| \n               | newline      |
| \t               | tab          |
| \'               | single quote |
| \"               | double quote |
| \\               | backslash    |

- You can also use a raw string, the syntax is:
```
print(r'a raw string \ \ \ \ \') # All the backslashes will render
```
- Wrapping your string in triple quotes will preserve newlines.
```
print("""Dear Alice,

I am using this letter as a way to show you how using triple quotes will format your strins"

Yours, 
Ri-Hani"""
```

## String CHeck Methods

| Method       | Checks if              |
|--------------|------------------------|
| isupper()    | string is uppercase    |
| islower()    | string is lower        |
| isalnum()    | string is alphanumeric |
| isdecimal()  | strung is numbers      |
| isalpha()    | string is letters      |
| isspace()    | string is whitespace   |
| istitle()    | string is titlecase    |
| startswith() | string starts with     |
| endswith()   | string ends with       |

## Lists in Python

- Lists are ordered sequences that can hold a variety of object types
- They use [] and , to deliminate the,
- Lists support indexing and slicing, they can be nested (a list inside a list) and have many useful methods that can be called for them
- Use the len() function to check the length of a list or a string
- You can concatenate lists with the + operator
- A list is not immutable, so you can redifine values at specific indeces
- .append method will append a new item to the end of the list
  - Append affects the values in the original list, so you don't have to assign the list to the output for it to be saved that way
- .pop metod will pop off an item at the end of the list, this will return the popped off item as well as mutate the list itself
  - You can assign the popped off item to a variable
  - You can specify which item to pop off by adding it's idndex as a parameter
- .sort This will not return anything but it will sort the list in place
- .reverse will reverse the list in place, will return None

## Dictionaries in Python

- Dictionaries are unordered mappings for objects, they use key-pair values as opposed to iterative indices
- We can use key-pair values to grab objects without needing to know an index location
- Dictionaries use curly braces instead of square brackets, and colons instead of commas
- When a list and when a dictionary?
  - Dictionaries have objects retrieved by key name
  - Unordered sequence, dictionaries can't be sorted
  - List objects are retrieved by location
  - Ordered sequence, lists can be sliced and sorted
- <code>my_dic{'key1':'value1', 'key2':'value2'}</code>
- To grab values use square brackets, but with the key instead of an index
  - <code>my_dic['key1']</code>
- .keys() returns the keys 
- .values() returns the values
- .items() returns the keys with the values
- Keys should always be strings

## Tuples in Python

- Tuples are like lists but they are immutable
- Once an element has been assigned you cannot change it
- Instead of [] we use ()
- A tuple can have multiple object types, it also supports slicing
- .index() returns the first index that the value you've passed in appears at
- .count(), shows how many times a value appears in a tuple
- Tuples provide data integrity by keeping you from reassigning elements

## Sets in Python

- Sets are unordered collections of unique elements, there can only be one representative of an object
- <code>myset= set()</code>
- .add() method will add an object to a set
- If you add a value that's already in a set it won't repeat it.
- You can cast a list to a set to get the unique values
- Sets don't have an order to them

## Booleans in Python

- Booleans convey true or false statements
- These are important for dealing with control flow and logic
- True must have a capitalized T and vice versa for False
- You can use None as a placeholder for a value that needs to be defined but whose value is not available yet


## I/O with Basic Files in Python

- open('myfile.txt')  You can assign the return value of this function to a variable
- Error 2 is a file not found, it's either a typo in the filename or an incorrect filepath
- .read() will return a giant string containing the text of the file, newline characters will be represented by '\n'
- If you read it again read's buffer will be at the end of the file. You will need to reset it to read it again, otherwise it will return an empty string
- .seek(INDEX) will seek the nth index of the large string that s your file's contents
- .readlines will return each line (delimited by new lines) as an object in a list
- If the file you want to open is not in the same dir as your script you need to provide the full file path to open, use '/' instead of '\' for MacOS and Linux
- .close will manually close your file
- You can use with in your syntax to assign your pen file to a variable and do whatever you want to the content, and python will close the file as soon as whatever you've specified is done

with open('myfile.txt') as myfile:
    contents = myfile.read()

- We can also write to and overwrite files, you can do this by changing the mode argument passed into open
  - mode=r read from files
  - mode=w overwrite files 9or create a new file)
  - mode=a append to files
- .write() same as read but for writing

## If, Elif and Else Statemens in Python

- Control flow is what we use to execute code only under certain conditions
- To control the flow of logic we use the keywords if, elif and else
- Control flow makes se of colons and indentation
- The indentation is crucial and is what sets Python apat from ther languages
```
if some_condtion:
    # execute some code
elif something_else:
    # execte some other code
else:
    # do something else
```

## While Loops in Python

- A while loop will execute it's block of code for as long as that condition is true
- You can use <code>break</code> or <code>continue</code> to force the loop to stop or continue regardless of the condition
- Break:
  - Immediately stops the loop without checking the condition and the executor moves om
- Continue
  - Continue will go back to the top of the loop and check the condition again
- Ctrl-C will break out of an infinite loop

## For Loops in Python

- Many objects in Python are iterable, e.g. an element in a lis or every character in a string
- We can use for loops to execute a block of code for every iteration
```
my_iterable = [1, 2, 3]
for item_name in my_iterable:
   printi(item_name)
```

- item_name can be any name, but my_list needs to be pre-defined

## Useful Operators

### Range

- range(start, stop, step (optional))
- If only one number is indicated the range will be from zero up to (but not including) the specfied number
- if 2 numbers are specified it will include the first number, upt to (but not including) the second number
- Range is a generator, and not a list in and of itself. To cast range to a list:
```
list(range(1, 11))
```
- This will eturn a list that starts from one and ends at 10
- A generator is a type of function that will generate information without savng it to memory

### Enumerate

```
word = 'hellodarkness blah blah'
for item in enumerate(word):
    print(item)
```
- This will print the index and letter at each index in <code>word</code>, the printed values will be tuples
```
(0, 'H')
```
- You can manipulate these tuples like you would any other
- Enumerate will take any iterable object and return an index counter as well as the object at that index

### zip

- The zip function will take any number of lists and pair them up by index to create tuples
- Zip will only be able to zip as many items as are in the shortest list, it will ignore anything extra
- Casting this into list will make a list of tuples

### In

- In can be used to check if something exists in a data structure
```
x in [1, 2, 3]
```
- This example will return false

| Operator | Meaning                                                          |
|----------|------------------------------------------------------------------|
| and      | Returns true if both arguments are true, otherwise returns false |
| or       | Returns true if either argument is true, otherwise returns false |
| not      | Returns the opposite of the argument, if false return true etc   |
| =        | Assigns the value on the right to the argument on the left       |
| ==       | Checks equality                                                  |
| >        | Greater than                                                     |
| <        | Less than                                                        |
| >=       | Greater than or equal to                                         |
| <=       | Less than or equal to                                            |

- The values 0, 0.0 and '' are considered falsey values. WHen used in a conditional they'll return false. All other values (in these types) are considered truthy

## Global and Local Scope

- Variables inside a function can have the same name as variables outside thhat function, but they are not considered to be the same variable, they have a different scope
- Variables inside a function have a local scope
- Variables outside a function have a global scope
- A scope covers an area of the source code
- Each function has its own local scope
- You can think of a scope as a container of variables
- Global scope is created when the program starts to run and is destroyed when the program has run its course
- Local scope is created when a function begins to run and exists for as long as the function is running, once the function returns the scope is destroyed and any variables are forgotten
- Code in a global scope can't use local variables
- Code in a local scope can access global variables
- Code in a function's local scope can't use variables from another function's local scope

## Error Handling with Try/Except

- By default a program will just crash if it hits an error
- Syntax:
```
try:
    a_block_of_code()
except AnErrorName:
    another_block_of_code()
```
## Multiple Assignment Trick

```
cat = ['large', 'orange', 'loud']
size = cat[0]
colour = cat[1]
volume = cat[2]
```
- The above code is equivalent to:
```
size, colour, volume = cat
```
- We cound also do the following:
```
size, colour, volume = "skinny", "black", "quiet"
```
- For a swap operation:
```
a, b = b, a
```

## List Methods

- index()
```
spam.index('hello')
```
- The above will return the index of the value 'hello' in the list spam
- If the value does not exist in the list it will return an exception
- If there are two identical values in the list the index method will return the index of the first one
- append()
```
spam.append('moose')
```
- This will append the value 'moose' to the end of the list spam
- insert()
```
spam.insert(1, 'chicken')
```
- This will insert the value 'chicken' at index 1 of the list spam
- These methods mutate the list and do not return a mutated list, they return None type
- The list is modified in place
- The above methods can only be called on list values
- remove()
```
spam.remove('bat')
```
- This will remove the value 'bat' from the list spam
- If there are multiple identical values this method will remove the first one
- sort()
```
spam.sort()
```
- If the list spam is made of numerical values this method will sort it numerically. If the list is made of strings it will sort it lexicographically
```
spam.sort(reverse=True)
```
- This will reverse the order of the sort
- If the list is a mix of types the method will raise an exception
```
spam.sort(key=str.lower)
```
- This will sort the list alphabetically and not lexicographically

## Regular Expressions

- The re module has the regular expression functions in it
```
RegeObject = re.compile(r'\d\d\d-\d\d\d-\d\d\d')
```
- This returns a Regex object, this has a search method
- In the example above the searth method will look for a pattern of 3 digits, a dash, three more digits, a dash and three last digits
```
RegeObject.search(message)
```
- The above will search the variable message for the pattern specified when making the regex object 
```
RegeObject.findall(message)
```
- This will all occurences of the pattern in message
- the .sub method will return the string but with the string you searched for replaced by a specified string

## Regex Groups and the Pipe CHaracter

- You can group your search pattersn by surrounding them in parenthesis in the string, then specifying which pattern you are looking or when calling the group method.
```
regObject = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d\d)')
mo = regObject.search('My number is 067-242-2863')
print(mo.group(1))
```
- The above will print '067'o- You can search for literal parentheses with backslashes in your regex pattern
- You can use pipes to denote variations in a searh pattern
```
regObject = re.compile(r'bat(mobile|erang|bat)')
```
- The above searches for batmobile, baterang, and batbat

## Repetition in Regex Expressions and Greedy/Nongreedy Matching

- the ? says the group matches 0 or 1 time
- the * says the group matches 0 or more times
- the + says the group mathces 1 or more times
- The curly braces can match a specific amount of times
- The curly braces with two numbers matches a number of times in the range of the numbers
- Leaving the first or secon number open says that there is no minimum or maximum
- Greedy matching matches the longest possible string, non greedy matching matches the shortest possible string
- Putting a question mark ofter curly braces makes it do non greedy matching

## Files

- The <code>os</code> module has lots of filepath string related methods that we can use
```
import os
```

```
os.path.join('folder1', 'folder2', 'folder3')
```
- This takes several string arguments and returns an OS appropriate string value of a path
```
os.gwtcwd()
```
- Returns a string value of a current working directory
```
os.chdir('\')
```
- Change the current working directory
- Absolute filepath: the complete filepath of a file or folder from the root directory
  - Starts with '/', '~', or 'C:\\', depending on the OS
- Relative filepath: a filepath that is relative to the current working directory
  - Starts with a './' or '../'
```
os.path.abspath()
```
- Will return a string absolute path of the path you pass it
```
os.path.isabs()
```
- Returns true if the path you pass it is absolute
```
os.path.relpath()
```
- Will return a relative path between two paths that you give it
```
os.path.dirname()
```
- Will retrieve the path up to the parent directory of the file/folder specified
```
os.path.basename
```
- Will retrieve the name of the file/dir specified
```
os.filepath.exists()
```
- Will return true if the specified filepath exists
- Will return false if this is a folder and not a file
```
os.path.getsize()
```
- Will return the size in bytes of the file as an int

## Reading and writing Plaintext Files

- There are three steps to reading and writing to files in python:

```
open('filename.txt')
```
- This will open the file in plaintext read mode
- When a file is open in readmode python will only let you read from the file
- This is the default mode for a file opened with this function
- The file datatype has several methods
```
filename.read()
```
- Will return a string of the entire file's contents
```
filename.close()
```
- This will close the opened file
- Once a file has been read, if we want to read it again we'll have to call open again.
```
filename.readlines()
```
- Will return all the lines as strings inside a list
- To write to a file with python you'll have to open the file in write mode or append mode.
- Write mode will overwrite the contents of the file, append mode will append to it
- To open write mode add 'w' as the second argument of open(), 'a' for append mode
- If the file does not exist python will open a new blank text file
```
filename.write('string')
```
- Will write string to the file filename
- Will return an int value of how many bytes were written to the file
- write does not automatically add newline characters
- If you want to save the contents of your file to a datastructure more complex than a string you can save variables to binary shelf files using the shelve module
```
impoert shelve
shelve.open('mydata')
```
- This will return a shelvefile object
- You can make changes to the shelve fileas if it were a dictionary, then call close on the shelve object

## Copying and moving files and folders

- The shutil module has methods that let you copy and move files and folders in python
```
import shutil
```
```
shutil.copy('spam.txt', '/')
```
- Will copy ./spam.txt to root
- If you specify a filename with the destination you will also rename the file
- copytree will recursively copy the contents of the folder you specify into the destination you specify
- You can use the move function to either move or rename a file or folder
- os.unlink() will delete a single file
- os.rmdir will delete a folder
- shutil.rmtree() will recursively delete a folder and it's contents

# Debugging
## the raise and assert Statements
- You can raise your own exceptions in python
- Exceptions are raised with a raise statement
```
raise Exception('This is the error message')
```
- This must be covered in a try except statement

## Logging

- We use logging to understand what''s happening in your program as well as the order it's happening in
- You can enable the logging module by importing the logging module
```
import logging(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
logging.basicConfig
```
- This is the setup code for logging in python
```
logging.debug('this step is being executed')
```
- The above format can be used to add log steps, these steps can be used to check values at different stages of a program
```
logging.disable(logging.CRITICAL)
```
- This line at the top of your file will diable log at the critical level or lower

- Log levels:
  - debug (lowest)
  - info
  - warning
  - error
  - critical (highest)
- Calling logging.debug() will create a logging message at the lowest level

## The webbrowser Module

```
import webbrowser
```
- Use to open urls

## The requests Module

- A third party module so you'll have to install it with pip
```
import requests
```
- Use to download files


## The Beautiful Soup Module

- Use to parse HTML
- Imported with the name bs4
- Pass the string with the html to the bs4.soup() function to get a soup object
- The soup object has a select() method that can be passed a css selector for the html tag

## Excel

- The OpenPyXL third party module allows you to open and manipulate excel spreadsheets
```
openpyxl.load_workbook('filename')
```
- Returns a workbook object for filename
- get_sheet_names() and get__sheet_by_name() gets worksheet objects
- sheet['A1'] will get a cell object for cell A1
- the cell() method also returns a cell from a sheet
- You can view and modify a sheet's name with it's title member variable
- Changing a cell's value is done by square brackets
- Changes can be saved with the save() method

## PDFs

- The PyPDF2 module can read and write PDFs
- You can open a pdf by passing it to open, and then passing that file object to PdfFileReader()
- You can obtain a page object with the getPage() method
- The text from a page object is obtained with the extractText() method, which can be buggy
- New PDFs can be made with PdfFileWriter
- New pages can be added with the addPage() method
- Call the write() method to save changes

## Sending Emails

- The protocol for sending emails is the SMTP protocol
- Send Mail Transfer Protocol
```
import smtplib
# Make a mail SMTP object which is listening on port 587
conn = smtplib.SMTP('smtp.gmail.com', 587)
# connect to the server, returns a response code and a bytes object
conn.elho()
# start tls encryption for when we send our password
conn.starttls()
# login
conn.login('email', 'password')
# send
conn.sendmail('from', 'to', 'Subject: subject')
# close connection to server
conn.quit()
```
## Controlling the Mouse with Python

- You can write programs that can send virtual keystrokes and mouse events to any GUI, this is known as GUI automation
- It's always better to use specific modules to control specific applications, but as a last resort you can always perform actions directly with the keyboard or mouse
- The module is pyautogui and needs to be installed with pip
- You can think of your screen as a graph with an x axis and a y axis, with 00 being in the top left hand of the screen, y coordinates increase going down and x coordinates increase going right
```
import pyautogui
# Get the resolution of the screen
pyautogui.size()
# Get the current coordinates of the mouse
pyautogui.position()
# move mouse to specific coordinates, and specify how many seconds you want it to take
pyautogui.moveTo(0, 0, duration=1.5)
# move mouse to a relative position
pyautogui.moveRel(20, 0) #moves mouse 20 positions to the right and 0 positions down
# perform a click
pyautogui.click(0, 0) # clicks the top left of the screen, there are also doubleClick(), right and middle click functions
```
- There are also drag functons

## Controlling the Keyboard

- PyautoGUI's keypresses will go to the window that currently has focus
- typewrite() can be passed a string for specific key presses, it also has an interval argument	
- pyautogui.press() can be used for individual presses
- pyautogui.hotkey can be used for shortcuts
