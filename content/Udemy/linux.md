---
title: "Lunix Administration"
date: 2020-03-05T14:34:58+02:00
draft: false
categories: ['Udemy']
displayInList: true
---

## Common Directories

```
/
```
- Root, the top of the file system heirarchy

```
/bin
```
- Binaries and other executable programs
```
/etc
```
- System configuration files
```
/home
```
- Home directories
```
/opt
```
- Optional/3rd party software
```
/tmp
```
- A temporary space, commonly cleared on reboot
```
/usr
```
- User related programs
```
/var
```
- Variable data, most notably used for logs
```
/cdrom
```
- Mount point for cd roms
```
/cgroup
```
- Control Groups heirarchy
```
/dev
```
- Device files, typically controlled by the operating system and the sys admins
```
/export
```
- Shared file systems
```
/lib
```
- System libraries
```
/lib64
```
- System libraries 64 bit
```
/lost+found
```
- Used by the filesystem to store recovered files after a file system check has been performed

## the Shell

- The shell is the default user interface
- Root is the superuser
- Root access is typically restricted to system admins
- Root access may be required to start, stop and install an application
- Day to day activities would be performed using a normal account
- ~expansion will normally expand to the home directory of the account

## Environment Variables

- Storage locations that are a name -value pair 
- Typically uppercase
- Access the contents with:
```
echo $var_name
```
- $PATH:
  - Controls the command search path
  - Contains a list of directories delimited by colons
```
ri-hani@rihani-INVALID:~$ echo $PATH
/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```
  - When a command is executed the kernel will look for the binary for that command in one of the above directories
```
which <command>
```
- The above will return the path to the requested command

## File and Directory Permission

- The first character of the output from ls -l will show what kind of file it is

| Symbol | Type          |
|--------|---------------|
|   -    | Normal file   |
|   d    | Directpry     |
|   l    | Symbolic Link |

- After this will be a series of 9 characters, 3 groups of 3, the characters will indicate what permissions have been given to which user. 
- The three groups of characters, in order, represent the permissions for the user, other and then groups
- The three permissions can have different effects for files vs directories

| Permission | File                           | Directory                                        |
|------------|--------------------------------|--------------------------------------------------|
| Read (r)   | Allows a file to be read       | Allows filenames in the directory to be read     |
| Write(w)   | Allows a file to be modified   | Allows entries to be modified within a dir       |
| Execute(x) | Allows the execution of a file | Allow acces to contents and metadata for entries |

- The categories of users are:
  - u: user
  - g: group
  - o: other
  - a: all
- Groups:
  - Every Linux user is in at least one group
  - Groups are used to organise users
  - the groups command displays a user's group
  - You can also use id -Gn

## Working with Groups

- New files belong to your primary group
- Use chgrp tp change a file's group

## Wildcards

- A character or a string used for patter matching
- Sometimes referred to as globs or glob patterns
- Globbing expands the wildcard pattern into a list of paths
- Wildcards can be used with most commands, if they accept a file or directory as an argument
- *
  - Matches 0 or more characters
```
ls \*.txt
```
- ?
  - Matches exactly one character
```
ls ?.txt
```
- Will display any files with one character precceding txt
- []
  - You can use a character class to make a very specific search pattern
  - Start with a ;eft bracket, add one or more characters you want to match, and then close the square brackets
```
ca[nt]\*
```
- The above will match:
  - cat
  - cant
  - catch
  - can
- To exclude characters use an excalamation mark before the pattern
- You can create a range by separating two characters with a hyphen
- Named charater classes:
  - Syntax:
```
[[:alpha:]]
```
  - alpha
  - alnum
  - digit
  - lower
  - space
  - upper
- If you want to find a wildcar character in a search pattern, escape that character with a backslash

## Input, Output and Redirection

- There are three I/O types:
  - Standard input stdin
  - Standard output stdout
  - Standard error stderr
- > Redirects output to a file
- >> Redirects output to a file and appends any existing content
- < redirects input from a file to a command

## Comparing files

```
diff file1 file2
```
- Compare two files
- Output:
```
3c3
```
- The first number is a line number from the first file, the second number is a line number from the second file, and the letter indicates the difference or lack theirof
- c = change
- a = action
- d - delete
```
sdiff file1 file2
```
- Side by side comparison
- Output:
```
line in file1 | line in file2
              > line only in file2
```
```
vimdiff file1 file2
```
- Will display the differences highlighted in vim

## Grep Options

- -i Case insensitive
- -c Count the number of occurences in a file
- -n Output with line numbers
- -v Invert match.Print lines that don't match

## The File Command

- file <file_name> <-- Will display the filetype

##Coying Files Over the Network

- To copy files over a network you will either need:
  - SCP - Secure Copy
  - SFTP SSH Transfer Protocol
- Both of the above are extentions of the secure shell protocol
```
scp source destination
```
- Copy files from source to destination
- You need to know what you want to copy first
```
sftp host
```
- Start a secure file transfer session with host
```
sftp jason@host
```
```
ftp host
```
- Start a file transfer session with host
- If ftp is enaabled
- Not a secure transfer protocol
- Login credentials are sent in plain text over the network
- Uploaded/downloaded files are not encrypted

## Customizing the Shell Prompt

- You can customize your shell by setting environment variables
- Bash, ksh, and sh use $PS1
- Csh, tcsh, and zsh use $prompt
- Format strings used to customize your prompt:
  - \d
    - Date in "WeekdayMonthDate" format
  - \h
    - Hostname up to the first period
  - \H
    - Hostname
  - \n
    - Newline
  - \t
    - Current time in 24h "HH:MM:SS" format
  - \T
    - Current time in 12h "HH:MM:SS" format
  - For a full list see the bash man page
- To make your custom shell persist, add the PS1 value to your personal initialization file
```
PS1 = "\u \w \t"
```

## Aliases

- Shortcuts
- Use for long commands
- Use for commands you use often
```
alias
```
- will display a list of current aliases
```
alias l="ls -l"
```
- ls -l will be mapped to l
- Can be used to fix common typos
- If you're more familiar with another OS's commands, you can map commands to their counterparts
```
unalias name
```
- Remove an alias
```
unalias -a
```
- Removes all aliases
- Add aliases to the .bash_profile file

## Environment Variables

- An environment variable is a storage location with a name/value pair
- THey can change an application's behaviour
```
EDITOR=vim
```
```
printenv
```
- Shows you all your environment variables
```
printenv name
```
- Will display the value of a specific environment variable
- Case sensitive, uppercase by convention
```
echo $HOME
```
- Will echo the contents of $Home
```
export EDITOR="nano"
```
- Will change the value of an environment variable
```
unset name
```
- Remove from your environment
- Save environment variables in .bash_profile

## Processes and Jobs

```
ps
```
- Display all running processes
- No option will display processes associated with your current session
- Options:
  - -e: everything
  - -f: full format listing
  - -u username: username's processes
  - -p pid: display information for PID
- Common ps commands:
  - ps -e
    - Show all processes
  - ps -ef
    - Show all processes full
  - ps -eH
    - Display a process tree
  - ps -e --forest
    - Display a process tree
  - ps -u username
    - Display user's process
- Other ways to view processes:
  - pstree
    - Display processes in a tree format
  - top
    - Interactive process viewer
  - htop
    - Interactive process viewer
- command &
  - Start command in background
- ctrl-c
  - Kill the foreground process
- ctrl-z
  - Suspend the foreground process
- A suspended process is a process that's not running in the background
- bg pid
  - Send a process to the background
- fg pid
  - Foreground a background process
- kill
  - Kill a process by name or pid
- jobs
  - List jobs

## Scheduling repeated Jobs with Cron

- Cron: a time-based scheduling service
- Crontab: a program to create, read, update, and delete your job schedules
- Use cron to schedule and automate tasks
- Cron checks for scheduled jobs every minute

- Crontab format:

  \*\*\*\*\*command
   | | | | |
   | | | | +-- Day of the week (0 - 6)
   | | | +---- Month of the year (1 - 12)
   | | +------ Day of the month (1 - 31)
   | +-------- hour (0 - 23)
   +---------- Minute (0 - 59)
- A crontab is a configuration file that specifies when commands are to be executed by cron
- Each line represents a job and contains: when to run and what to run
```
# Run every Monday at 07:00
0 7 \* \* 1 /opt/sales/bin/weekly-report
```
- See man cron for some system specific cron shortcuts
```
crontab file
```
- Install a new crontab from a file
```
crontab -l
```
- List your cron jobs
```
crontab -e
```
- Edit your cron jobs
```
crontab -r 
```
- Remove all your cron jobs

##The Linux Boot Process

### BIOS
- Basic input output system
- Special firmware used in the booting process
- The first piece of software run when a computer is powered on
- Operating system independant
- Primary purpose is to test the underlying hardware components and to find and execute the boot loader
- Performs the POST
- Power On Self Test
  - Performs basic checks on various hardware components
- The BIOS will only attempt to load the boot loader if the POST succeeds
- Has a list of bootable devices e.g. hard drive, usb drive, dvd drive etc
- The boot device order can be changed
- Once the bootable device has been found, the BIOS will run the bootloader

### Bootloader
- GRUB is typical, but some older systems may use the LILO loader
- Grand Unified Bootloader
- Linux Loader
- Bootloaders start the operating system
- Bootloaders can start the operating system with different options

### Initial RAM Disk
- initrd
  - initial ram disk
- A temporary file system that is loaded from disk and stored in memory
- Contains helpers and modules used to load the permanent filesystem
- Once the initrd loads the permanent file system it's job is done, the OS continues loading from the real file system

### The /boot Directory
- ./boot
  - Contains files required to boot linux
  - kernel
  - initrd
  - boot loader config

### Kernel Ring Buffer

- Contains messages related to the Linux kernel
- A datastructure that is always the same size
- Once the buffer is full, old messages will be discarded when new messages arrive
```
dmesg
```
- Show the contents of the kernel buffer
- Most distros will also store this on disk at /var/log/dmesg

### Runlevels/Description
- Linux uses runlevels to determine which processes to start
- Each system can be configured individually, but here are some typical runlevels:
  - 0       Shuts down the system
  - 1, S, s Single user mode, used for maintenance
  - 2       Multiuser mode with GUI (Debian/Ubuntu)
  - 3       Multiuser text mode (RedHat/CentOS)
  - 4       Undefined
  - 5       Multiuser mode with GUI (RedHat/CentOs)
  - 6       Reboot

### Init

- Traditionally controlled runlevels
- Being phased out by systemd

### Systemd

- Most widely adopted alternative to initd
- Uses targets instead of runlevels

## System Logging

### The Syslog Standard

- Aids in the processing of messages
- Allows logging to be centrally controlled
- Uss facilities and severities to categorixe messages
- Facilities are used to indicate what type of program or what part of the system the message originated from
- E.g. Messages that are labeled with the kern facility oringinate from the Linux kernel
-----------------------------------------------------
|Number | Keyword | Description                     |
|-------|---------|---------------------------------|
| 0     | kern    | kernel messages                 |
| 1     | user    | mail systems                    |
| 2     | mail    | mail system                     |
| 3     | damon   | system daemons                  |
| 4     | auth    | security/authorization messages |
| 5     | syslog  | messages generated by syslogd   |
| 6     | lpr     | line printer subsystem          |
| 7     | news    | network news subsystem          |
| 8     | uucp    | UUCP subsystem                  |
| 9     | clock   | daemon                          |
| 10    | authpriv| security/athorization messages  |
| 11    | FTP     |  FTP daemon                     |
| 12    | -       |  NTP subsystem                  |
| 13    | -       | lpg audit                       |
| 14    | -       | log alert                       |
| 15    | cron    | clock daemon                    |
| 16    | local0  | use 0 (local0)                  |
| 16    | local1  | use 0 (local1)                  |
| 16    | local2  | use 0 (local2)                  |
| 16    | local3  | use 0 (local3)                  |
