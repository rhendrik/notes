---
title: "Ansible"
date: 2020-02-04T12:18:41+02:00
draft: false 
categories: ['Udemy']
displayInList: true
---

Make it Work 

Make it Right

Make it Fast

## Configuration Management & Orchestration

Configuration management tools provide you with abstactions that make your automation processes better. Configration management is about taking your host from an unknown state and getting it into a desired end-state.

Orchestration allows you to manage configuration managements accross multiple hosts.

Ansible is procedural and ordered, this allows you to orchestrate hosts correctly, and it also has configuration management modules.

## Preperation for Ansible
## Environment Set-Up

A comon 3-tier typology:

<table>
  <th colspan="3">
  Container
  </th>
  <tr>
    <td>Loadbalancer</td>
    <td>Webserver</td>
    <td>Database</td>
  </tr>
</table>

<em> Control Machine</em> - this acts as the orchestration hub it is where we wll install ansible

