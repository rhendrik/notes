---
title: "YAML - YouTube (Giraffe Academy)"
date: 2020-02-18T11:49:53+02:00
draft: false
categories: ['YouTube']
displayInList: true
---

YAML:

YAML
Ain't
Markup
Language

- YAML is used to store information about different things
- We can use it to define key value pairs
- Very similar to JSON
- YAML places a huge emphasis on readability and user-friendliness
- Can be defined with either a .yml or a .yaml extension
- Commens are defined with a '#'
- YAML lets you define objects, lists and key-value pairs.
- Syntax for a key-value pair:
```
name: "Ri-Hani"
```
- Key-value pairs are stored inside of objects 
```
   person:
     name: "Ri-Hani"
```
- Scope is defined by indentation
- We can define lists in a few ways
```
list:
  -name: "steph"
   age: 22
```
```
list:
  -{name: "steph", age: 22}
```
- When using a large amount of text we can specify whether or not we want to render i all as one line, or to keep the formatting
```
description: < #This wll render the text that follows (which would contain indents and newlines for readability) as one line
```
```
description: | #This will preserve any formatting when the text is being rendered
```
- It is possible to anchor either the value in a key-value pair, or the contents of an object
```
name: &name "Ri-Hani"

id: *name
```
```
person: &details
  name: "Ri-Hani"
  age: 22

details: 
  <<: *details
```

