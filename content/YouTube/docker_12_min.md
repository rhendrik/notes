---
title: "Docker in 12 Min"
date: 2020-02-05T09:12:15+02:00
categories: ['YouTube']
displayInList: true
draft: false 
---

Docker is a tool for running applications in an isolated environment.

Some benefits are:

1. You are always running aplications in the same envronment. This means that you don't need to worry about inconsistencies. It always acts the same, regardless of who is running it where.
2. It allows you to sandbox projects so that you can keep their environments compatmentalized.
3. It makes it easier to work on other people's projects without having to worry about dependencies.

Docker gives these advantages without the overhead and hassle of running a VM.

## Containers

Instead of running a VM we use containers. A container is not a full VM.

When running a vrtual machine that machine is given a full operating system including a kernel.

<cite>The kernel is a computer program at the core of a computer's operating system with complete control over everything in the system. - Wikipedia</cite>

This is quite resource heavy on the host machine. A container will use the host machine's kernel, therefore resources are shared. Docker uses special features of the UNIX file system to create these isolated environments.

As a result, a container can start up in seconds as opposed to minutes. They use fewer resources, taking up less disk space and using less memory.

## Images

A container is a running instance of an image. An image is a template of the environment that you want to snapshot of the system at a particular time. he image has the operating system, the software and the applications all bundled into it.

Images are deined using a dockerfile. A dockerfile s a text fle with a list of steps to create said image.

Make dockerfile --> run to build an image --> run to make containers

## Docker Filesystem - Union File System

Docker images are stored as a series of read-only layers. When we start a container, docker takes a read-only image and adds a read-write layer on top. If the running container modifies an existing file Docker copies that file from the read-only layer into the read-write layer on top. This version hides the underlying read-only file, but does not destroy it. When a docker container is deleted, relaunching the image will start a fresh new docker container wth none of the previous modifications made in the previous container. these changes are lost forever. this combination of read-only and read-write layers are called a Union File System.

## Volumes

Volumes were created by docker for the purpose of saving modified data while also sharing data between containers. Volumes are directries or files that exist outside of the default Union File system and on the host's file system.

The most direct way to declare a volume is to do so at runtime with the -v flag.

<code>docker run -it --name vol-test -h CONTAINER -v /data debian /bin/bash</code>

This will make the file <code>data</code> exist outside the the Union file system and directly on the host. We can find out where on the host the file lives by using the <code>docker inspect</code> command on the host. This must be executed in a seperate terminal from the one running the container.

<code>docker inspect -f "{{json .Mounts}}" vol-test | jq .</code>

You should see something like:

docker inspect -f "{{json .Mounts}}" vol-test | jq .

(out) [

(out)   {

(out)     "Name": "8e0b3a9d5c544b63a0bbaa788a250e6f4592d81c089f9221108379fd7e5ed017",

(out)     "Source": "/var/lib/docker/volumes/8e0b3a9d5c544b63a0bbaa788a250e6f4592d81c089f9221108379fd7e5ed017/_data",

(out)     "Destination": "/data",

(out)     "Driver": "local",

(out)     "Mode": "",

(out)     "RW": true,

(out)     "Propagation": ""

(out)   }

(out) ]


