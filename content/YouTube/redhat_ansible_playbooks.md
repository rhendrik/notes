---
title: "Redhat Ansible Playbooks"
date: 2020-02-19T14:49:20+02:00
categories: ['YouTube']
displayInList: true
draft: false
---

## Variables

- Ansible can work with metadata from various sources and manage their context in the form of variables
- Variables in Ansible are used to account for differences between services
- Variables can be defined as needed in various hostvars or group vars files

### Variable Precedence

- Variable precedence in which the same variables are going to override eachother
- There are 16 levels of variable precedence
1. Extra vars
2. Task vars (only for the task)
3. Block vars (only for the blocks in the tasks)
4. Role and include vars
5. Play vars_files
6. Play vars_prompt
1. Play vars
1. Set facts
1. Registered vars
1. Host facts
1. playbook host_vars
1. Playbook group_vars
1. Inventory host_vars
1. Inventory group_vars
1. Inventory vars
1. Role defaults
- Where you define the variables in the inventory will affect where the variables fall on the precedence chain
- Think about where you define your variables in relaton to what your playbook is doing
