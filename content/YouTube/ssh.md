---
title: "SSH"
date: 2020-02-04T13:19:32+02:00
draft: false
categories: ['YouTube']
displayInList: true
---

SSH = Secure Shell

<cite> Secure Shell (SSH) is a cryptographic network protocol for operating network services securely over an unsecured network. - <em>Wikipedia</em> </cite>

SSH is a communication protocol, much like HTTPS, ftp etc. You can use it to do pretty much anything you'd be able to do through a command-line on a remote computer. All traffic will be encrypted. it is normally used through the terminal/command-line.

## Client/Server Communication

SSH is the client, and the server/machine you are trying to connect to must have SSHD (Open SSH Daemon) installed and rnning to be able to connect to it with an SSH key.

<cite>SSHD sits and listens for SSH connections</cite>

Most servers you connect tp will be Linux, and most Linux servers will have SSHD installed. This server will have an SSHD config file, which you can edit to your desired specs.

### Authentication Methods

- Password

This is the default authentication method.

You would have the user on the remote server and you would have the password for that user.

<code>ssh brad@123.456.789</code>

The host machine can be accessed either with the I.P. (as shown above) or the hostname. If SSH keys are not set up you will then be prompted for the user's password.

- Public/Private Key pair

This method bypasses the need fr a password entirely. You would do this by generating a set of SSH keys, either public or private.

Using SSH keys is a much safer method than using passwords. Passwords can be brute force attacked etc.

- Host Based

Host-based authentication goes by a host file which will store any host allowed to connect to that machine.

### Generating Keys

<code>ssh-keygen</code>

To generate an SSH key run the above command. This will generate a private and public key.

These keys will be automatically generated and stored in the following:

 - <code>~/.ssh/id_rsa</code> (private)
 - <code>~/.ssh/id_rsa.pub</code> (public)

Your server/machine will either already have a .ssh folder, or you will have to make one. nside this folder will be a file named *authorized keys*, which will be where your public key will be stored. This is how the machine knows which machines are auhorized to login.

## SSH for Github

1. Generate a new SSH key

2. Go to your github, go to settings and add a new ssh key.
  - Give your Key a name
  - copy the contents of the .pub file that was generated into the empty field

3. Eval the SSH agent
<code>eval `ssh-agent`</code> this will return the agent PID
4. Use <code>ssh-add</code> and the file containing the *private* key to add it to our machine. this command wll return a confirmation that the key has been added
 
From here on any repos set with SSH instead of HTTPS will be accessible through this machine
