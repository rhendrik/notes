---
title: "Ansible Playbooks (YouTube)"
date: 2020-02-19T07:47:43+02:00
draft: false 
categories: ['YouTube']
displayInList: true
---

- Playbooks are a set of instructions written in YAML, they're ansible's orchestration language
- Play books are divided into plays, a play defines a set of tasks to be run on the host
- Plays define what actions you want to take on which hosts, the hosts are defined in an inventory
- Plays are dictionaries, i.e. a list of objects and key-value pairs
- A task is an acton to be performed on a host, for example executing a command, running a script, etc
- The order of plays don't matter, but tasks are executed in sequence so their order does matter, eg you can't run an applicaton before you've installed it
- The host parameter in the playbook defines which host you are performing your play's actions on
- Playbook format:
```
---
-
  name: Play1
  hosts: localhost
  tasks:
    - name: Execute date command
      command: date

    - name: Execute script
      script: test_script.sh

-
  name: Play2
  yum:
    name: httpd
    state: present
```
- The different actions run by tasks are called modules
```
ansible-playbook <playbook file name>
```
