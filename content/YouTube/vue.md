---
title: "Vue (YouTube)"
date: 2020-02-27T09:36:27+02:00
draft: false
categories: ['YouTube']
displayInList: true
---

- Vue is a JS framework used for building UIs and front-end applications
- It features a virtual DOM
- Used to build powerful SPAs
- It might help to look into:
  - ES6 Moduke syntax
  - Higher order array methods
  - Arrow functions
  - Fetch API & promises
- Vue uses UI components

## Anatomy of a Component

- Output
```
<template
  <div class ="user">
    <h1>{{ user.name }}</h1>
  </div>
</template>
```
- Functionality
```
<script>
  export default {
    name: "User",
    data () {
      return {
        user: { name: "Brad" }
      }
    }
  }
</script>
```
- Style
  - he scoped component means that this style will only apply to this particular component
```
<style scoped>
  h1 {
    font-size: 2rem;
  }
</style>
```

## Vue CLI

- The vue cli allows you to run commands either from the command line or through a GUI
- Using the GUI gives a bit more control

## Commands:

```
vue create <NAME>
```
- This will generate the basics for your new application

```
npm run serve
```
- We can use this to start our server
- You will be able to access your app on localhost:8080

```
vue ui
```
- This will start the vue gui which will be accessible on port 8000
- This gui is a project manager
- You'll be able to configure your vue cli from this gui

## Anatomy of a Generated Vue site

### package.json

- holds metadata around dependancies, parser information, plugins, versions etc
- Holds scripts such as serve, build and lint

### pblic/index.html

- This is the page that s served in the brwser
- It will contain a dic or other element that contains our components

### src/main.js

- This is the entry point to vue
- It imports vue as well as our main app component (which was automatically generated)
- It renders our app component in a dic with the id #app

### src/App.vue

- The main application component, it consists of:
  - a template
  - A script secton
  - A style section (global styling)
- There will be a Hello World component imbedded in the html, much like an html tag
```
<template>
  <div id="app">
    <HelloWorld msg="Never Gonna Give You up"/>
  </div>
</template>

```
- You can add props to your componenst by using an html attribute syntax, eg ms="Rick Rolled" will display msg as an H1
- The component will be imported in the script section of the .vue file
```
<script>
import HelloWorld from './components/HelloWorld.vue'

export default {
  name: 'App',
  components: {
    HelloWorld
  }
}
</script>

```
- All components accessed by the export object and must live in src/components/component.vue

### src/components/HelloWorld.vue

- This is the default component generated when running vue build <NAME>
  - All components to be embedded will live in this directory
- Any props for this component will be embedded in th script section of this file
- In the style section of this file, if you'd like the css to only apply to this component, you can add the word scoped to the opening style tag

## Directives and V-if

- Directives allow us to add special reactivity to pieces of the rendered DOM
- The <code>v-</code> syntax (eg v-bind) denites a vue directive in the HTML
- <code>v-if</code> allows us to achieve a conditional rendering in vue

## V-for

- Syntax:
```
<ul>
  <li v-for="colour in colours"></li>
</ul>
```
- The above will render all items in the array colours
```
<ul>
  <li v-for="colour.text in colours" v-if.primary></li>
</ul>
```
- The above will display the colour.text items in the colours object, and only if colour.primary is true

## V-on

- Syntax:
```
<button v-on:click="reveal">A Button</button>
```
- In the example above, when a user clicks the button the code will call the reveal method
- The shorthand for <code>v-on</code> is @

## V-model and Computed property

- Models allow the user to update state in the component with something like an input field
- A computed property allows us to compute some data with complex logic
- v-model syntax:
```
<div id="app">
  <span>Change the Text</span>
  <input v-model="text" placeholder="text...">
</div>
```
- The above example will create an input field that will send the text entered to the variable text, with a placeholder of text... showing in the input field

## Lifecycle Hooks

