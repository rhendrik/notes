---
title: "Docker Compose in 12 Min (YouTube)"
date: 2020-02-19T09:04:01+02:00
draft: false
categories: ['YouTube']
displayInList: true
---

- Dcker compose lets us define our services in a config file
- dcker-compose files are written in YAML
- The first line in your docker compose file should specify the version, typically this will be 3, this doesn't refer to the version of compose that you have installed, rather it's the version of compose fle format you wish to use
- Next we specify the services object, this object can be named anything (eg product services)
- If you need to specify a directory (for example for a script you need to run) the path wll be relative to the compose file itself
- Volumes are specified as a YAML list, as are ports
- The contents of a compose file are things you could have specified when running the build command, but in a way that can be consistently used again and again
- To run yur compose file (this must be run in the same dir as the compose file:
```
docker-compose up
```

```
version: "3"
services:
  product-service:
    build: ./product
    volumes:
      - ./product:/usr/src/app
    ports:
      - 5001:80
```
